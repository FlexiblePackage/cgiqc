﻿using System;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Reflection;

namespace cgi_qc
{
    internal class Program
    {

        double wait = 0;
        private Program()

        {
            wait = Convert.ToDouble(File.ReadAllText("delay.ini").Trim());
        }

        private string Decrypt(byte[] data)
        {
            using (var aes = new RijndaelManaged())
            {
                aes.Key = Encoding.ASCII.GetBytes("camtester-220803");
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.Zeros;
                var cryptoTransform = aes.CreateDecryptor();
                var resultArray = cryptoTransform.TransformFinalBlock(data, 0, data.Length);
                var output = Encoding.ASCII.GetString(resultArray);
                return output;
            }
        }

        private void Connect(string ip, ref Socket socket, ref IPEndPoint client, ref EndPoint host)
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            client = new IPEndPoint(IPAddress.Parse(ip), 80);
            host = (EndPoint)new IPEndPoint(IPAddress.Any, 0);

            socket.Connect(client);
        }

        private string getOEM(string ip)
        {
            string result = "";

            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                var client = new IPEndPoint(IPAddress.Parse(ip), 80);
                var host = (EndPoint)new IPEndPoint(IPAddress.Any, 0);

                try
                {
                    socket.Connect(client);

                    var buf = Encoding.ASCII.GetBytes("GET /goform/getOemInfo?videoflicker=&countrycode=&brandname=&partnerid=&checksum8715=&hardwareid= HTTP/1.1\r\nAuthorization: Basic YWRtaW46YWRtaW4=\r\nHost:" + ip + "\r\nConnection: Keep-Alive\r\n" + "\r\n");
                    var sendBuf = socket.SendTo(buf, client);
                    Thread.Sleep(TimeSpan.FromSeconds(wait));

                    byte[] rev = new byte[1024];
                    var err = socket.ReceiveFrom(rev, ref host);
                    var temp = Encoding.ASCII.GetString(rev);
                    result = Decrypt(rev);
                }
                catch (SocketException)
                {
                    Console.WriteLine("Bad Connection");
                    return "Bad Connection";
                }

                return result;
            }
        }

        private string getCA(string ip)
        {
            string result = "";

            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                var client = new IPEndPoint(IPAddress.Parse(ip), 80);
                var host = (EndPoint)new IPEndPoint(IPAddress.Any, 0);

                try
                {
                    socket.Connect(client);

                    var buf = Encoding.ASCII.GetBytes("GET /get_ca_checksum HTTP/1.1\r\nAuthorization: Basic YWRtaW46YWRtaW4=\r\nHost:" + ip + "\r\nConnection: Keep-Alive\r\n\r\n");
                    var sendBuf = socket.SendTo(buf, client);
                    Thread.Sleep(TimeSpan.FromSeconds(wait));

                    byte[] rev = new byte[1024];
                    var err = socket.ReceiveFrom(rev, ref host);
                    var temp = Encoding.ASCII.GetString(rev);
                    result = Decrypt(rev);
                }
                catch (SocketException)
                {
                    Console.WriteLine("Bad Connection");
                    return "Bad Connection";
                }

                return result;
            }
        }

        private string getCH(string ip)
        {
            string result = "";

            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                var client = new IPEndPoint(IPAddress.Parse(ip), 80);
                var host = (EndPoint)new IPEndPoint(IPAddress.Any, 0);

                try
                {
                    socket.Connect(client);

                    var buf = Encoding.ASCII.GetBytes("GET /get_channel_plan HTTP/1.1\r\nAuthorization: Basic YWRtaW46YWRtaW4=\r\nHost:" + ip + "\r\nConnection: Keep-Alive\r\n\r\n");
                    var sendBuf = socket.SendTo(buf, client);
                    Thread.Sleep(TimeSpan.FromSeconds(wait));

                    byte[] rev = new byte[1024];
                    var err = socket.ReceiveFrom(rev, ref host);
                    var temp = Encoding.ASCII.GetString(rev);
                    result = Decrypt(rev);
                }
                catch (SocketException)
                {
                    Console.WriteLine("Bad Connection");
                    return "Bad Connection";
                }

                return result;
            }
        }


        static void Main(string[] args)
        {
            //args =new string[]{ "192.168.50.11", "hello"};

            if (args.Length > 0)
            {
                var ip = args[0].Trim();
                var sn = args[1].Trim();

                string result = "pass";

                Program p = new Program();
                var oem = p.getOEM(ip);
                var ca = p.getCA(ip);
                var ch = p.getCH(ip);
                var sum = oem + ca + ch;

                var compare = File.ReadAllText("compare.ini").Split(new string[] { "\r\n" }, StringSplitOptions.None);
                var match = new string[8]
                {
                "BrandName:\\w+",
                "CountryCode:\\w+",
                "PartnerID:\\w+",
                "VideoServiceFW:\\w+",
                "VideoServiceFWVersion:\\w+.\\w+.\\w+",
                "HardwareID:\\w+",
                "CA_CheckSum:\\w+",
                "ChannelPlan:\\w+"
                };

                var date = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                var path = File.ReadAllText("path.ini").Trim();
                using (var sw = new StreamWriter(path + "/" + sn + "," + date + ".log"))
                {
                    Console.WriteLine("Date:" + date);
                    for (var i = 0; i < match.Length; i++)
                    {
                        var temp = Regex.Match(sum, match[i]).Value;
                        Console.WriteLine(temp);
                        sw.WriteLine(temp);

                        if (compare[i] != temp)
                            result = "fail";
                    }

                    Console.WriteLine("result:" + result);
                    sw.WriteLine("result:" + result);
                }
            }
            else
            {
                Console.WriteLine("cgi_qc ver:1.0.0");
                Console.WriteLine("press any key to quit");
                Console.ReadLine();
            }
        }
    }
}
